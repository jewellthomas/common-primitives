import unittest
import os
import re

from common_primitives import dataset_regex_filter
from d3m import container, exceptions


class RegexFilterPrimitiveTestCase(unittest.TestCase):
    def test_inclusive(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': True,
            'regex': 'AAA'
        })

        filter_primitive = dataset_regex_filter.RegexFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        matches = new_dataset['learningData'][new_dataset['learningData']['code'].str.match('AAA')]
        self.assertTrue(matches['code'].unique() == ['AAA'])

    def test_exclusive(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': False,
            'regex': 'AAA'
        })

        filter_primitive = dataset_regex_filter.RegexFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        matches = new_dataset['learningData'][~new_dataset['learningData']['code'].str.match('AAA')]
        self.assertTrue(set(matches['code'].unique()) == set(['BBB', 'CCC']))

    def test_row_metadata_removal(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset.metadata = dataset.metadata.update(('learningData', 1), {'a': 0})
        dataset.metadata = dataset.metadata.update(('learningData', 2), {'b': 1})

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': False,
            'regex': 'AAA'
        })

        filter_primitive = dataset_regex_filter.RegexFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        # verify that the lenght is correct
        self.assertEquals(len(new_dataset['learningData']), new_dataset.metadata.query(('learningData',))['dimension']['length'])

        # verify that the rows were re-indexed in the metadata
        self.assertEquals(new_dataset.metadata.query(('learningData', 0))['a'], 0)
        self.assertEquals(new_dataset.metadata.query(('learningData', 1))['b'], 1)
        self.assertFalse('b' in new_dataset.metadata.query(('learningData', 2)))

    def test_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': None,
            'column': 1,
            'inclusive': True,
            'regex': 'AAA'
        })

        filter_primitive = dataset_regex_filter.RegexFilterPrimitive(hyperparams=hp)
        with self.assertRaises(exceptions.InvalidArgumentValueError):
            filter_primitive.produce(inputs=dataset)

    def test_bad_regex(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': True,
            'regex': '['
        })

        filter_primitive = dataset_regex_filter.RegexFilterPrimitive(hyperparams=hp)
        with self.assertRaises(exceptions.InvalidArgumentValueError):
            filter_primitive.produce(inputs=dataset)

    def test_can_accept_success(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'resource_id': '0',
            'inclusive': True,
            'regex': '[12345]'
        })

        accepted_metadata = dataset_regex_filter.RegexFilterPrimitive.can_accept(
            method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
        )

        self.assertIsNotNone(accepted_metadata)

    def test_can_accept_bad_regex(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': '1',
            'column': 1,
            'inclusive': True,
            'regex': '\\gg12345]'
        })

        with self.assertRaises(re.error):
            dataset_regex_filter.RegexFilterPrimitive.can_accept(
                method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
            )

    def test_can_accept_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_2', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': None,
            'column': 1,
            'regex': 'abcde',
            'inclusive': True
        })

        with self.assertRaises(ValueError):
            dataset_regex_filter.RegexFilterPrimitive.can_accept(
                method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
            )

    def test_can_accept_bad_regex(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_regex_filter.RegexFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': '1',
            'column': 1,
            'inclusive': True,
            'regex': '['
        })

        with self.assertRaises(re.error):
            dataset_regex_filter.RegexFilterPrimitive.can_accept(
                method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
            )


if __name__ == '__main__':
    unittest.main()
