import unittest
import os

from common_primitives import dataset_numeric_range_filter
from d3m import container

import utils as test_utils


class NumericRangeFilterPrimitiveTestCase(unittest.TestCase):
    def test_inclusive_strict(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 6.5,
            'max': 6.7,
            'strict': True,
            'inclusive': True,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        new_dataframe = filter_primitive.produce(inputs=dataset).value['learningData']

        self.assertGreater(new_dataframe['sepalLength'].astype(float).min(), 6.5)
        self.assertLess(new_dataframe['sepalLength'].astype(float).max(), 6.7)

    def test_inclusive_permissive(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 6.5,
            'max': 6.7,
            'strict': False,
            'inclusive': True,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        new_dataframe = filter_primitive.produce(inputs=dataset).value['learningData']

        self.assertGreaterEqual(new_dataframe['sepalLength'].astype(float).min(), 6.5)
        self.assertLessEqual(new_dataframe['sepalLength'].astype(float).max(), 6.7)

    def test_exclusive_strict(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter \
            .NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 6.5,
            'max': 6.7,
            'strict': True,
            'inclusive': False,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        new_dataframe = filter_primitive.produce(inputs=dataset).value['learningData']

        self.assertEqual(
            len(new_dataframe.loc[
                (new_dataframe['sepalLength'].astype(float) >= 6.5) &
                (new_dataframe['sepalLength'].astype(float) <= 6.7)]), 0)

    def test_exclusive_permissive(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter \
            .NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 6.5,
            'max': 6.7,
            'strict': False,
            'inclusive': False,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        new_dataframe = filter_primitive.produce(inputs=dataset).value['learningData']

        self.assertEqual(
            len(new_dataframe.loc[
                (new_dataframe['sepalLength'].astype(float) > 6.5) &
                (new_dataframe['sepalLength'].astype(float) < 6.7)]), 0)

    def test_row_metadata_removal(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        # add metadata for rows 0 and 1
        dataset.metadata = dataset.metadata.update(('learningData', 0), {'a': 0})
        dataset.metadata = dataset.metadata.update(('learningData', 5), {'b': 1})

        # apply filter that removes rows 0 and 1
        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 0,
            'min': 1,
            'max': 4,
            'strict': True,
            'inclusive': False,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        # verify that the rows were re-indexed in the metadata
        self.assertEquals(new_dataset.metadata.query(('learningData', 0))['a'], 0)
        self.assertEquals(new_dataset.metadata.query(('learningData', 1))['b'], 1)
        self.assertFalse('b' in new_dataset.metadata.query(('learningData', 5)))

    def test_bad_type_handling(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_numeric_range_filter \
            .NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 6.5,
            'max': 6.7,
            'strict': False,
            'inclusive': False,
            'resource_id': 'learningData'
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)
        with self.assertRaises(ValueError):
            filter_primitive.produce(inputs=dataset)

    def test_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_2', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_numeric_range_filter \
            .NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 5,
            'min': 45.0,
            'max': 100.0,
            'strict': False,
            'inclusive': False,
            'resource_id': None
        })
        filter_primitive = dataset_numeric_range_filter.NumericRangeFilterPrimitive(hyperparams=hp)

        with self.assertRaises(ValueError):
            filter_primitive.produce(inputs=dataset)

    def test_can_accept_success(self):
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 0.0,
            'max': 1.0,
            'strict': True,
            'inclusive': True,
            'resource_id': 'learningData'
        })

        accepted_metadata = dataset_numeric_range_filter.NumericRangeFilterPrimitive.can_accept(
            method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
        )

        self.assertIsNotNone(accepted_metadata)

    def test_can_accept_bad_range(self):
        dataset = test_utils.load_iris_metadata()

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 1.0,
            'max': 0.0,
            'strict': True,
            'inclusive': True,
            'resource_id': 'learningData'
        })

        accepted_metadata = dataset_numeric_range_filter.NumericRangeFilterPrimitive.can_accept(
            method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
        )

        self.assertIsNone(accepted_metadata)

    def test_can_accept_bad_type(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 0.0,
            'max': 1.0,
            'strict': True,
            'inclusive': True,
            'resource_id': 'learningData'
        })

        accepted_metadata = dataset_numeric_range_filter.NumericRangeFilterPrimitive.can_accept(
            method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
        )

        self.assertIsNone(accepted_metadata)

    def test_can_accept_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_2', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_numeric_range_filter.NumericRangeFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'min': 0.0,
            'max': 1.0,
            'strict': True,
            'inclusive': True,
            'resource_id': None
        })

        with self.assertRaises(ValueError):
            dataset_numeric_range_filter.NumericRangeFilterPrimitive.can_accept(
                method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
            )


if __name__ == '__main__':
    unittest.main()
