import unittest

import numpy as np
from d3m.container.numpy import ndarray

from common_primitives.diagonal_mvn import DiagonalMVN, Hyperparams as MVNHyperparams, Params as MVNParams
from common_primitives.linear_regression import LinearRegression, Hyperparams, Params


class TestLinearRegression(unittest.TestCase):
    def test_fit(self):
        # Calculate fit using the primitives
        regularizer = DiagonalMVN(hyperparams=MVNHyperparams.defaults())
        regularizer.set_params(params=MVNParams(mean=ndarray([0.0, 0.0, 0.0], generate_metadata=True),
                                                covariance=ndarray(np.identity(3)*1.0, generate_metadata=True)))

        hy = Hyperparams.defaults()
        hy1 = dict(hy)
        hy1['weights_prior'] = regularizer
        hy2 = Hyperparams(hy1)

        # setting to 0 to force grad opt
        hy1['analytic_fit_threshold'] = 0
        hy1['learning_rate'] = 0.001

        hy3 = Hyperparams(hy1)

        linreg1 = LinearRegression(hyperparams=hy2)
        linreg2 = LinearRegression(hyperparams=hy3)

        # TODO make another test with a larger matrix (P > N)

        train_y = ndarray([-2, -1, 0, 1, 2, 3], generate_metadata=True)
        train_x = ndarray([[10, 9, 8], [3.5, 3, 0], [7, 2, 1], [8, 1, 3], [9, 0, 13], [9, 0, 13]], generate_metadata=True)
        linreg1.set_training_data(outputs=train_y, inputs=train_x)
        linreg2.set_training_data(outputs=train_y, inputs=train_x)

        linreg1.fit(iterations=1)
        # test conitnue fit here
        while (linreg2.get_call_metadata().has_finished == False):
            linreg2.fit(iterations=100)

        w_analytic = linreg1.get_params()['weights']
        w_gradient = linreg2.get_params()['weights']

        self.assertTrue(linreg1.get_call_metadata().iterations_done == 1)
        self.assertTrue(linreg2.get_call_metadata().has_finished == True)
        self.assertTrue(linreg1.get_call_metadata().has_finished == True)
        # print(np.linalg.norm(w_gradient - w_analytic))
        self.assertTrue(np.linalg.norm(w_gradient-w_analytic) < 0.02)

    def test_batches(self):
        hy = Hyperparams.defaults()
        hy1 = dict(hy)
        # forces it to use non analytic
        hy1['analytic_fit_threshold'] = 0
        hy1['batch_size'] = 3

        hy2 = Hyperparams(hy1)

        linreg = LinearRegression(hyperparams=hy2)
        # train_x = np.array([[9, 8], [3, 2], [7, 2], [5, 4], [5, 5], [9, 3]])
        train_x = np.array([[3, 4], [-3, -2], [0, -2], [2, -3], [1, 1], [-3, 2]])
        weights = np.array([1, 0.4])
        train_y = np.dot(train_x, weights) + 0.000001*np.random.randn(len(train_x))
        linreg.set_training_data(inputs=train_x, outputs=train_y)
        linreg.fit(iterations=10000)
        pred_weights = linreg.get_params()['weights']
        self.assertTrue(
            np.linalg.norm(weights-pred_weights)
            < 0.2)

    def test_gradients(self):
        linreg = LinearRegression(hyperparams=Hyperparams.defaults())
        linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True),
                                        offset=0,
                                        noise_variance=2,
                                        weights_variance=ndarray(np.diag([0.00001, 0.00001, 0.00001]), generate_metadata=True)))
        grad = linreg.gradient_output(outputs=np.array([3]), inputs=np.array([[1, 1]]))[0]
        # TODO finite differencesd test instead to be sure

        # https://www.wolframalpha.com/input/?i=d%2Fdy(-+(y-2)%5E2+)%2F(2*2)+at+y+%3D+3
        # print(grad)
        expected_grad = -0.5

        self.assertEqual(grad, expected_grad)

        # next test grad_params similarly...

    def test_sample(self):
        linreg = LinearRegression(hyperparams=Hyperparams.defaults())
        linreg.set_params(params=Params(weights=ndarray([1, 1], generate_metadata=True),
                                        offset=0,
                                        noise_variance=0.00001,
                                        weights_variance=ndarray(np.diag([0.00001, 0.00001, 0.00001]), generate_metadata=True)))

        # TODO t-test
        # Check second sample is within 50 SDs of the expected value.
        sample = linreg.sample(inputs=np.array([[3, 1]]), num_samples=2).value
        self.assertTrue(3.5 < sample[0][0] < 4.5)

        # check second sample is within 50 SDs of the expected value
        assert 3.5 < sample[1][0] < 4.5

    def test_backward(self):
        hy = Hyperparams.defaults()
        hy1 = dict(hy)
        hy1['analytic_fit_threshold'] = 0
        hy1['learning_rate'] = 0.001
        #  hy1['weights_prior'] = regularizer
        # TODO when mvn is ready to accept a backward call uncomment this
        hy2 = Hyperparams(hy1)

        linreg = LinearRegression(hyperparams=hy2)
        # train_x = np.array([[9, 8], [3, 2], [7, 2], [5, 4], [5, 5], [9, 3]])
        train_x = np.array([[3, 4], [-3, -2], [0, -2], [2, -3], [1, 1], [-3, 2]])
        weights = np.array([1, 2])
        train_y = np.dot(train_x, weights) + 0.000001*np.random.randn(len(train_x))

        linreg.set_training_data(inputs=train_x, outputs=train_y)
        linreg.fit(iterations=20000)

        # beginning of end to end test
        p = linreg.produce(inputs=np.array([[3, 1], [5, 9]])).value

        assert p[0] - 5 < 1
        assert p[1] - 23 < 1

        # make up some end to end loss and its grad
        # TODO finite differences here as well
        grad_loss = ndarray([0.6, -1.9], generate_metadata=True)
        #grad_inputs, grad_params = linreg.backward(gradient_outputs=grad_loss, fine_tune=True)

        fitted = linreg.get_params()

        # this is fine tune false
        #  assert np.linalg.norm(fitted['weights'] - weights) = 0
        # TODO seperate out fine tune false and true

    def test_log_likelihood(self):
        linreg = LinearRegression(hyperparams=Hyperparams.defaults())
        linreg.set_params(params=Params(weights=ndarray([1, 1]),
                                        offset=0,
                                        noise_variance=0.0001,
                                        weights_variance=ndarray([[1, 0, 0], [0, 1, 0], [0, 0, 1]], generate_metadata=True)))
        assert linreg.log_likelihood(outputs=ndarray([2.1, 3.1], generate_metadata=True),
                                     inputs=ndarray([[1, 1], [2, 1]], generate_metadata=True)).value >\
            linreg.log_likelihood(outputs=ndarray([2.2, 3], generate_metadata=True),
                                  inputs=ndarray([[1, 1],[2, 1]], generate_metadata=True)).value
        # TODO rewrite this test


if __name__ == '__main__':
    unittest.main()
