import os
import typing

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

import pandas as pd  # type: ignore

__all__ = ('NumericRangeFilterPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[typing.Union[str, None]](
        default=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of column to filter if there are multiple tabular resources inside a Dataset.',
    )
    column = hyperparams.Hyperparameter[int](
        default=-1,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Index of column filter applies to.'
    )
    inclusive = hyperparams.Hyperparameter[bool](
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='True when values outside the range are removed, False when values within the range are removed.'
    )
    min = hyperparams.Hyperparameter[float](
        default=-float("inf"),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Minimum value for filter.'
    )
    max = hyperparams.Hyperparameter[float](
        default=float("inf"),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Maximum value for filter.'
    )
    strict = hyperparams.Hyperparameter[bool](
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='True when the filter bounds are strict (ie. less than), false then are not (ie. less than equal to).'
    )


class NumericRangeFilterPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which filters rows from a dataset based on a numeric range applied to a given column.
    Columns are identified by their index, and the filter itself can be inclusive (values within range are retained)
    or exclusive (values within range are removed).  Boundaries values can be included in the filter (ie. <=) or excluded
    (ie. <).
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '8b1c1140-8c21-4f41-aeca-662b7d35aa29',
            'version': '0.1.0',
            'name': "Numeric range filter",
            'python_path': 'd3m.primitives.data_preprocessing.numeric_range_filter.DatasetCommon',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:cbethune@uncharted.software',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_numeric_range_filter.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_PREPROCESSING,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        try:
            resource_id, resource = base_utils.get_tabular_resource(inputs, self.hyperparams['resource_id'], pick_entry_point=False)
        except ValueError as error:
            raise exceptions.InvalidArgumentValueError("Failure to find tabular resource '{resource_id}'.".format(resource_id=self.hyperparams['resource_id'])) from error

        # to make sure index matches row indices
        resource = resource.reset_index(drop=True)

        # apply the filter using native dataframe methods
        col_idx = self.hyperparams['column']
        try:
            to_keep: pd.Series
            if self.hyperparams['inclusive']:
                if self.hyperparams['strict']:
                    to_keep = (resource.iloc[:, col_idx].astype(float) > self.hyperparams['min']) & \
                        (resource.iloc[:, col_idx].astype(float) < self.hyperparams['max'])

                else:
                    to_keep = (resource.iloc[:, col_idx].astype(float) >= self.hyperparams['min']) & \
                        (resource.iloc[:, col_idx].astype(float) <= self.hyperparams['max'])
            else:
                if self.hyperparams['strict']:
                    to_keep = (resource.iloc[:, col_idx].astype(float) < self.hyperparams['min']) | \
                        (resource.iloc[:, col_idx].astype(float) > self.hyperparams['max'])
                else:
                    to_keep = (resource.iloc[:, col_idx].astype(float) <= self.hyperparams['min']) | \
                        (resource.iloc[:, col_idx].astype(float) >= self.hyperparams['max'])

            to_keep_indices = resource.loc[to_keep].index

        except ValueError as error:
            raise exceptions.InvalidArgumentValueError(
                "Failure to apply numerical range filter to column {col_idx} of type {type}.".format(
                    col_idx=col_idx,
                    type=resource.iloc[:, col_idx].dtype,
                ),
            ) from error

        # remove dataframe and metadata rows by index
        outputs = inputs.select_rows({resource_id: to_keep_indices})

        return base.CallResult(outputs)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        # make sure the resource exists
        resource_id = base_utils.get_tabular_resource_metadata(inputs_metadata, hyperparams['resource_id'], pick_entry_point=False)

        # make sure the column has a numeric semantic type
        semantic_types = inputs_metadata.query((resource_id, metadata_base.ALL_ELEMENTS, hyperparams['column'])).get('semantic_types', [])
        if not semantic_types:
            return None
        if 'http://schema.org/Float' not in semantic_types and 'http://schema.org/Integer' not in semantic_types:
            return None

        # make sure min/max values make sense
        if hyperparams['min'] > hyperparams['max']:
            return None

        return inputs_metadata
