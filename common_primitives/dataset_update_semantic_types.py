import copy
import typing
import os

from d3m import container, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('UpdateSemanticTypesPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[typing.Union[str, None]](
        default=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of columns to update semantic types for if there are multiple tabular resources inside a Dataset.'
    )
    add_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='A set of column indices of columns to add semantic types for. Semantic types to add are in "add_types".',
    )
    add_types = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Semantic types to add for columns listed in "add_columns".',
    )
    remove_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='A set of column indices of columns to remove semantic types from. Semantic types to remove are in "remove_types".',
    )
    remove_types = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Semantic types to remove from columns listed in "remove_columns".',
    )


class UpdateSemanticTypesPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which adds and remove semantic types for columns in a DataFrame inside a Dataset.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '98c79128-555a-4a6b-85fb-d4f4064c94ab',
            'version': '0.2.0',
            'name': "Semantic type updater",
            'python_path': 'd3m.primitives.data_transformation.update_semantic_types.DatasetCommon',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:cbethune@uncharted.software',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_update_semantic_types.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id, resource = base_utils.get_tabular_resource(inputs, self.hyperparams['resource_id'], pick_entry_point=False)

        outputs = copy.copy(inputs)

        outputs.metadata = self._update_metadata(outputs.metadata, resource_id, self.hyperparams)

        return base.CallResult(outputs)

    @classmethod
    def _update_metadata(cls, inputs_metadata: metadata_base.DataMetadata, resource_id: metadata_base.SelectorSegment, hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        outputs_metadata = inputs_metadata

        for column_index in hyperparams['add_columns']:
            for semantic_type in hyperparams['add_types']:
                outputs_metadata = outputs_metadata.add_semantic_type((resource_id, metadata_base.ALL_ELEMENTS, column_index), semantic_type)

        for column_index in hyperparams['remove_columns']:
            for semantic_type in hyperparams['remove_types']:
                outputs_metadata = outputs_metadata.remove_semantic_type((resource_id, metadata_base.ALL_ELEMENTS, column_index), semantic_type)

        return outputs_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        resource_id = base_utils.get_tabular_resource_metadata(inputs_metadata, hyperparams['resource_id'], pick_entry_point=False)

        return cls._update_metadata(inputs_metadata, resource_id, hyperparams)
