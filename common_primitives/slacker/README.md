This package contains Slacker modules code copied as they are from existing D3M datasets' solutions,
with the following changes:
* `base` module import modified to be a relative import.
* Commented out any prints.
* In `DataFrameCategoricalEncoder` made sure regular dicts are stored in `code_maps` and not `defaultdict`.
  This makes pickling possible.

Some solutions contain slightly modified versions, but these files here match the most common ones.
