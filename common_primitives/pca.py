from typing import Dict, Tuple, Optional

import os
import torch  # type: ignore
import numpy as np  # type: ignore

from d3m.container import ndarray
from d3m.primitive_interfaces.base import PrimitiveBase, CallResult, DockerContainer
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils

import common_primitives

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.


class Params(params.Params):
    transformation: ndarray
    n_components: int


class Hyperparams(hyperparams.Hyperparams):
    max_components = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0,
        description="max number of principled componenets to fit with"
    )
    proportion_variance = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
        default=1.0,
        description="regularization argument"
    )


def remove_mean(data: torch.Tensor) -> Tuple:
    """
    input: NxD torch array
    output: D-length mean vector, NxD torch array

    takes a torch tensor, calculates the mean of each
    column and subtracts it

    returns (mean, zero_mean_data)
    """

    N, D = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += row.view(D)/N
    zero_mean_data = data - mean.view(1, D).expand(N, D)
    return mean, zero_mean_data


class PCA(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    # TODO implement as probabilistic pca -> add in probabilistic mixin

    """
    Example of a dimensionality reduction primitive.
    """

    __author__ = "Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
        "id": "1ca1285d-af54-47d3-abfe-42b22924bb3f",
        "version": "0.1.0",
        "name": "Principal Component Analysis",
        "source": {
            'name': common_primitives.__author__,
            'contact': 'mailto:willh@robots.ox.ac.uk',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/pca.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        "installation": [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        "python_path": "d3m.primitives.feature_extraction.pca.Common",
        "algorithm_types": ['PRINCIPAL_COMPONENT_ANALYSIS'],
        "primitive_family": "FEATURE_EXTRACTION",
    })

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        """
        If both max_components and proportion_variance are set, the one which
        gives the fewest principal components is used.
        """
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._max_components = hyperparams['max_components']
        self._n_components = 0
        self._proportion_variance = hyperparams['proportion_variance']
        self._training_inputs = None  # type: Optional[int]
        self._fitted = False
        self._transformation = None   # type: Optional[torch.Variable]
        self._mean = None             # type: Optional[torch.Variable]

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        "PCA with a variance heuristic to select the number of components"
        # If already fitted with current training data, this call is a noop.
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        # Get eigenvectors of covariance
        self._mean, zero_mean_data = remove_mean(self._training_inputs)
        cov = torch.from_numpy(np.cov(self._training_inputs.numpy().T))  # type: ignore
        cov = cov.type(torch.FloatTensor)
        e, V = torch.eig(cov, True)
        # Choose which/how many eigenvectors to use
        total_variance = sum(np.linalg.norm(e.numpy()[i, :])
                             for i in range(e.size()[0]))
        indices = []
        self._n_components = 0
        recovered_variance = 0
        while recovered_variance < self._proportion_variance * total_variance \
                and (self._max_components == 0 or self._n_components < self._max_components):
            best_index = max(range(e.size()[0]), key=lambda x: np.linalg.norm(e.numpy()[x, :]))
            indices.append(best_index)
            recovered_variance += np.linalg.norm(e.numpy()[best_index, :])
            e[best_index, :] = torch.zeros(2)
            self._n_components += 1

        # Construct transformation matrix with eigenvectors
        self._transformation = torch.zeros(
                [self._n_components, self._training_inputs.size()[1]]  # type: ignore
            ).type(torch.DoubleTensor)
        for n, index in enumerate(indices):
            self._transformation[n, :] = V[:, index]

        self._fitted = True
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        "Returns the latent matrix"
        inputs = torch.from_numpy(np.array(inputs)).type(torch.DoubleTensor)
        comps = np.array([torch.mv(self._transformation, row - self._mean).numpy() for row in inputs])
        return CallResult(comps)

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        self._training_inputs = torch.from_numpy(inputs).type(torch.DoubleTensor)
        self._fitted = False
        self._n_components = None

    def get_params(self) -> Params:
        return Params(transformation=ndarray(self._transformation.numpy(), generate_metadata=True),
                      n_components=self._n_components)

    def set_params(self, *, params: Params) -> None:
        self._transformation = torch.from_numpy(params.transformation).type(torch.DoubleTensor)
