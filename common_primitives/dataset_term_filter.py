import os
import typing
import re

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('TermFilterPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[typing.Union[str, None]](
        default=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of column to filter if there are multiple tabular resources inside a Dataset.',
    )
    column = hyperparams.Hyperparameter[int](
        default=-1,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Index of column filter applies to.',
    )
    inclusive = hyperparams.Hyperparameter[bool](
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='True when values that contain a match against the term list are retained, False when they are removed.',
    )
    terms = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='A set of terms to filter against. A row will be filtered if any term in the list matches.',
    )
    match_whole = hyperparams.Hyperparameter[bool](
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='True if a term is matched only against a full word, False if a word need only contain the term.',
    )


class TermFilterPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which filters rows from a dataset based on a column value containing a match
    against a caller supplied term list.  Supports search-style matching where the target need only
    contain a term, as well as whole word matching where the target is tokenized using regex word boundaries.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '622893c7-42fc-4561-a6f6-071fb85d610a',
            'version': '0.1.0',
            'name': "Term list dataset filter",
            'python_path': 'd3m.primitives.data_preprocessing.term_filter.DatasetCommon',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:cbethune@uncharted.software',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_term_filter.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_PREPROCESSING,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        try:
            resource_id, resource = base_utils.get_tabular_resource(inputs, self.hyperparams['resource_id'], pick_entry_point=False)
        except ValueError as error:
            raise exceptions.InvalidArgumentValueError("Failure to find tabular resource '{resource_id}'.".format(resource_id=self.hyperparams['resource_id'])) from error

        try:
            escaped_terms = [re.escape(t) for t in self.hyperparams['terms']]

            if self.hyperparams['match_whole']:
                # convert term list into a regex that matches whole words
                pattern = re.compile(r'\b(?:%s)\b' % '|'.join(escaped_terms))
            else:
                # convert term list into a regex that does a partial match
                pattern = re.compile('|'.join(escaped_terms))

            matched = resource.iloc[:, self.hyperparams['column']].str.contains(pattern)
            to_keep = matched if self.hyperparams['inclusive'] else ~matched

            to_keep_indices = resource.loc[to_keep].index

        except re.error as error:
            raise exceptions.InvalidArgumentValueError("Failed to compile regex for terms: {terms}".format(terms=self.hyperparams['terms'])) from error

        # remove dataframe and metadata rows by index
        outputs = inputs.select_rows({resource_id: to_keep_indices})

        return base.CallResult(outputs)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        # make sure the resource exists
        base_utils.get_tabular_resource_metadata(inputs_metadata, hyperparams['resource_id'], pick_entry_point=False)

        return inputs_metadata
