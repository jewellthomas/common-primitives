import collections
import typing

from d3m import container, exceptions
from d3m.container import pandas as container_pandas
from d3m.metadata import base as metadata_base


# This is just a workaround for use with d3m v2019.4.4 core package.
# See: https://gitlab.com/datadrivendiscovery/d3m/merge_requests/196
#      https://gitlab.com/datadrivendiscovery/common-primitives/issues/68
# TODO: Remove it when used with future versions.
def get_relations_graph(inputs: container.Dataset) -> typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]]:
    """
    Builds the relations graph for the dataset.

    Each key in the output corresponds to a resource/table. The value under a key is the list of
    edges this table has. The edge is represented by a tuple of four elements. For example,
    if the edge is ``(resource_id, True, index_1, index_2, custom_state)``, it
    means that there is a foreign key that points to table ``resource_id``. Specifically,
    ``index_1`` column in the current table points to ``index_2`` column in the table ``resource_id``.

    ``custom_state`` is an empty dict when returned from this method, but allows users
    of this graph to store custom state there.

    Returns
    -------
    Dict[str, List[Tuple[str, bool, int, int, Dict]]]
        Returns the relation graph in adjacency representation.
    """

    graph: typing.Dict[str, typing.List[typing.Tuple[str, bool, int, int, typing.Dict]]] = collections.defaultdict(list)

    for resource_id in inputs.keys():
        if not issubclass(inputs.metadata.query((resource_id,))['structural_type'], container_pandas.DataFrame):
            continue

        columns_length = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS,))['dimension']['length']
        for index in range(columns_length):
            column_metadata = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, index))

            if 'foreign_key' not in column_metadata:
                continue

            if column_metadata['foreign_key']['type'] != 'COLUMN':
                continue

            foreign_resource_id = column_metadata['foreign_key']['resource_id']

            # "COLUMN" foreign keys should not point to non-DataFrame resources.
            assert isinstance(inputs[foreign_resource_id], container_pandas.DataFrame), type(inputs[foreign_resource_id])

            if 'column_index' in column_metadata['foreign_key']:
                foreign_index = column_metadata['foreign_key']['column_index']
            elif 'column_name' in column_metadata['foreign_key']:
                foreign_index = inputs.metadata.get_column_index_from_column_name(column_metadata['foreign_key']['column_name'], at=(foreign_resource_id,))
            else:
                raise exceptions.UnexpectedValueError("Invalid foreign key: {foreign_key}".format(foreign_key=column_metadata['foreign_key']))

            # "True" and "False" implies forward and backward relationships, respectively.
            graph[resource_id].append((foreign_resource_id, True, index, foreign_index, {}))
            graph[foreign_resource_id].append((resource_id, False, foreign_index, index, {}))

    return graph
