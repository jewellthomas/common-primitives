import copy
import os
import typing
import itertools

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('RemoveColumnsPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[typing.Union[str, None]](
        default=None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Resource ID of columns to remove if there are multiple tabular resources inside a Dataset.'
    )
    columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='A set of column indices of columns to remove.'
    )


class RemoveColumnsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which removes columns from a DataFrame inside a Dataset. Columns are specified by index.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '2eeff053-395a-497d-88db-7374c27812e6',
            'version': '0.2.0',
            'name': "Column remover",
            'python_path': 'd3m.primitives.data_transformation.remove_columns.DatasetCommon',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:cbethune@uncharted.software',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/dataset_remove_columns.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id, resource = base_utils.get_tabular_resource(inputs, self.hyperparams['resource_id'], pick_entry_point=False)

        keep_columns = self._get_columns(inputs.metadata, resource_id, self.hyperparams)

        outputs = copy.copy(inputs)

        # Update data.
        resource = resource.iloc[:, list(keep_columns)]
        outputs[resource_id] = resource

        # Update metadata.
        outputs.metadata = self._select_columns_metadata(outputs.metadata, resource_id, keep_columns)

        return base.CallResult(outputs)

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, resource_id: metadata_base.SelectorSegment, hyperparams: Hyperparams) -> typing.Sequence[int]:
        all_columns = range(inputs_metadata.query((resource_id, metadata_base.ALL_ELEMENTS))['dimension']['length'])
        keep_columns = [column_index for column_index in all_columns if column_index not in hyperparams['columns']]
        return keep_columns

    @classmethod
    def _select_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata, resource_id: metadata_base.SelectorSegment,
                                 columns: typing.Sequence[int]) -> metadata_base.DataMetadata:
        """
        This is similar to ``select_columns_metadata`` but operates on a Dataset.
        """

        if not columns:
            raise exceptions.InvalidArgumentValueError("No columns selected.")

        # This makes a copy so that we can modify metadata in-place.
        output_metadata = inputs_metadata.update(
            (resource_id, metadata_base.ALL_ELEMENTS,),
            {
                'dimension': {
                    'length': len(columns),
                },
            },
        )

        if resource_id is metadata_base.ALL_ELEMENTS:
            metadata_chain = itertools.chain(
                [output_metadata._current_metadata.all_elements.all_elements if output_metadata._current_metadata.all_elements is not None else None],
                output_metadata._current_metadata.all_elements.elements.values() if output_metadata._current_metadata.all_elements is not None else iter([None]),
            )
        else:
            resource_id = typing.cast(metadata_base.SimpleSelectorSegment, resource_id)

            metadata_chain = itertools.chain(
                [output_metadata._current_metadata.all_elements.all_elements if output_metadata._current_metadata.all_elements is not None else None],
                output_metadata._current_metadata.all_elements.elements.values() if output_metadata._current_metadata.all_elements is not None else iter([None]),
                [output_metadata._current_metadata.elements[resource_id].all_elements],
                output_metadata._current_metadata.elements[resource_id].elements.values(),
            )

        # TODO: Move this to metadata API.
        for element_metadata_entry in metadata_chain:
            if element_metadata_entry is None:
                continue

            elements = element_metadata_entry.elements
            new_elements_evolver = d3m_utils.EMPTY_PMAP.evolver()
            for i, column_index in enumerate(columns):
                if column_index in elements:
                    # If "column_index" is really numeric, we re-enumerate it.
                    if isinstance(column_index, int):
                        new_elements_evolver.set(i, elements[column_index])
                    else:
                        new_elements_evolver.set(column_index, elements[column_index])
            element_metadata_entry.elements = new_elements_evolver.persistent()
            element_metadata_entry.is_elements_empty = not element_metadata_entry.elements
            element_metadata_entry.update_is_empty()

        # TODO: Update boundary columns and "confidence for" references.

        return output_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        resource_id = base_utils.get_tabular_resource_metadata(inputs_metadata, hyperparams['resource_id'], pick_entry_point=False)

        keep_columns = cls._get_columns(inputs_metadata, resource_id, hyperparams)

        return cls._select_columns_metadata(inputs_metadata, resource_id, keep_columns)
