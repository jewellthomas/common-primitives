from typing import Sequence, Dict

import os
import time
import tempfile

try:
    open('~/.theanorc', 'a+')
except FileNotFoundError:
    compile_dir = tempfile.TemporaryDirectory()
    os.environ['THEANO_FLAGS'] = 'base_compiledir=' + compile_dir.name

import numpy as np  # type: ignore
from pymc3 import Model, Normal, Bernoulli, NUTS  # type: ignore
from pymc3 import invlogit, sample, sample_ppc, find_MAP  # type: ignore
import pymc3 as pm  # type: ignore
from pymc3.backends.base import MultiTrace  # type: ignore

import common_primitives

from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin
from d3m.primitive_interfaces.base import SamplingCompositionalityMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer, Gradients
import theano  # type: ignore
from pymc3.theanof import gradient, CallableTensor  # type: ignore


# n_inputs x n_dimensions array of floats
Inputs = ndarray
# 1D int array of length n_inputs
Outputs = ndarray


class Params(params.Params):
    weights: MultiTrace


class Hyperparams(hyperparams.Hyperparams):
    burnin = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1000,
        description='The number of samples to take before storing them'
    )
    mu = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0.0,
        description='Mean of Gaussian prior on weights'
    )
    sd = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1.0,
        description='Standard deviation of Gaussian prior on weights'
    )

# TODO
# - should implement the gradient mixin
# backward :
#   - in the fine tuning use the gradient wrt all the sampled weights
# gradient_ouputs :

# TODO
# support the loss mixin


class BayesianLogisticRegression(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                 SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                 # Base class should be after all mixins.
                                 SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping logistic regression using PyMC3 and its
    Theano backend.
    """

    __author__ = "Oxford DARPA D3M Team, Rob Zinkov <zinkov@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
        "id": "87dfa2c3-d0de-48aa-b3e5-814496ae211e",
        "version": "0.1.0",
        "source": {
            'name': common_primitives.__author__,
            'contact': 'mailto:zinkov@robots.ox.ac.uk',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/logistic_regression.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        'installation': [{
            # Used by Theano.
            'type': metadata_module.PrimitiveInstallationType.UBUNTU,
            'package': 'libopenblas-dev',
            'version': '0.2.20+ds-4',
        }, {
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                git_commit=utils.current_git_commit(os.path.dirname(__file__)),
            ),
        }],
        "name": "common_primitives.BayesianLogisticRegression",
        "python_path": "d3m.primitives.classification.bayesian_logistic_regression.Common",
        "algorithm_types": ["MARKOV_CHAIN_MONTE_CARLO"],
        "primitive_family": "CLASSIFICATION",
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        self._burnin = self.hyperparams['burnin']
        self._mu = self.hyperparams['mu']
        self._sd = self.hyperparams['sd']
        self._seed = -1
        self._fitted = False
        self._training_inputs = None  # type: ndarray
        self._training_outputs = None  # type: ndarray
        self._trace = None  # type: MultiTrace
        self._model = None  # type: Model

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """
        Sample a Bayesian Logistic Regression model using NUTS to find
        some reasonable weights.
        """
        if iterations is None:
            iterations = 1000

        if timeout is None:
            timeout = np.inf

        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        # make sure that all training outputs are either 0 or 1
        if not ((self._training_outputs == 0) | (self._training_outputs == 1)).all():  # type: ignore
            raise ValueError("training outputs must be either 0 or 1")

        # batch_size = 2000
        # samples_left = iterations - batch_size
        # start = time.time()

        # training data needs to be a Theano shared variable for
        # the later produce code to work
        _, n_features = self._training_inputs.shape
        self._training_inputs = theano.shared(self._training_inputs)
        self._training_outputs = theano.shared(self._training_outputs)

        # As the model depends on number of features it has to be here
        # and not in __init__
        with Model() as model:
            weights = Normal('weights', self._mu, self._sd, shape=n_features)
            p = invlogit(pm.math.dot(self._training_inputs, weights))
            Bernoulli('y', p, observed=self._training_outputs)
            trace = sample(iterations,
                           random_seed=self._seed,
                           trace=self._trace,
                           tune=self._burnin, progressbar=False)

        self._trace = trace
        # while time.time() < start + timeout and samples_left > 0:
        #     samples_left -= batch_size
        #     if samples_left < batch_size:
        #         batch_size = samples_left
        #
        #     with model:
        #         trace = sample(batch_size,
        #                        trace=trace, progressbar=False)
        #     self._trace = trace

        self._model = model
        self._fitted = True
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        "Returns the MAP estimate of p(y | x = inputs; w)"
        w = self._trace['weights']
        y = (np.einsum('kj,ij->i', w, inputs) > 0).astype(int)
        return CallResult(y)

    def sample(self, *,
               inputs: Inputs, num_samples: int = 1,
               timeout: float = None, iterations: int = None) -> CallResult[Sequence[Outputs]]:
        # Set shared variables to test data, outputs just need to be
        # the correct shape
        self._training_inputs.set_value(inputs)
        self._training_outputs.set_value(np.random.binomial(1, 0.5, inputs.shape[0]))

        with self._model:
            post_pred = sample_ppc(self._trace,
                                   samples=num_samples,
                                   progressbar=False)
        return CallResult(post_pred['y'].astype(int))

    def _log_likelihood(self, *, input: Inputs, output: Outputs) -> float:
        """
        Provides a likelihood of one output given the inputs and weights.

        mathcal{L}(y | x; w) = log(p(y | x; w)) = log(p) if y = 0 else log(1 - p)
        where p = invl(w^T * x)
              invl(x) = exp(x) / 1 + exp(x)
        """

        logp = self._model.logp
        weights = self._trace["weights"]
        self._training_inputs.set_value(input)
        self._training_outputs.set_value(output)
        return float(np.array([logp(dict(y=output,
                                         weights=w)) for w in weights]).mean())

    def log_likelihoods(self, *,  # type: ignore
                        outputs: Outputs,
                        inputs: Inputs,
                        timeout: float = None,
                        iterations: int = None) -> CallResult[Sequence[float]]:
        "Provides a likelihood of the data given the weights"
        return CallResult(np.array([self._log_likelihood(input=np.array([input]),
                                                         output=np.array([output]))
                                    for input, output in zip(inputs, outputs)]))

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Params]:
        # dlogp = CallableTensor(gradient(logpt, vars))
        # dlogp(inputs)
        pass

    def get_params(self) -> Params:
        return Params(weights=self._trace)

    def set_params(self, *, params: Params) -> None:
        self._trace = params.weights

    def set_random_seed(self, *, seed: int) -> None:
        self._seed = seed
