import hashlib
import os
import typing
from typing import Any, Dict, List, Tuple

import d3m.metadata.base as metadata_module
import numpy as np  # type: ignore
import pandas as pd  # type: ignore

from d3m import exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.container.pandas import DataFrame
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces.base import CallResult
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase

import common_primitives

Inputs = DataFrame
Outputs = DataFrame


class Params(params.Params):
    categories: Dict[int, Dict[int, Any]]
    unseen: List[Tuple[int, str]]
    missing: Dict[str, List[int]]
    fitted: bool


class Hyperparams(hyperparams.Hyperparams):
    separator = hyperparams.Hyperparameter[str](
        default='.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Separator separates additional identifier and original column name",
    )
    prefix = hyperparams.Hyperparameter[str](
        default='col',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Separator separates additional identifier and original column name",
    )
    use_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to force primitive to operate on. If any specified column cannot "
                    "be used, it is skipped.",
    )
    exclude_inputs_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of inputs column indices to not operate on. Applicable only if \"use_columns\" is not "
                    "provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed "
                    "columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )
    handle_unseen = hyperparams.Enumeration(
        values=['error', 'ignore', 'handle'],
        default='ignore',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="error: throw exception when unknown value observed"
                    "ignore: ignore unseen values"
                    "auto: put unseen values in extra column and mark the cell as 1"
    )
    handle_missing_value = hyperparams.Enumeration(
        values=['error', 'ignore', 'column'],
        default='ignore',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='Options for dealing with missing values.'
                    'error: throw exceptions when missing values encountered.'
                    'ignore: ignore any missing value.'
                    'column: add one column for missing value.'
    )
    missing_values = hyperparams.Hyperparameter[typing.Sequence[Any]](
        default=[np.NaN, None],
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Separator separates additional identifier and original column name",
    )


class OneHotMakerPrimitive(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Attempts to detect discrete values in data and convert these to a
    one-hot embedding.
    """
    _Unseen_Column_Prefix: str = 'Unseen'
    _Missing_Column_Prefix: str = 'Missing'

    metadata = metadata_module.PrimitiveMetadata({
        'id': 'eaec420d-46eb-4ddf-a2cd-b8097345ff3e',
        'version': '0.2.0',
        'name': 'One-hot maker',
        'keywords': ['data processing', 'one-hot'],
        'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:willh@robots.ox.ac.uk',
            'uris': [
                'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/one_hot_maker.py',
                'https://gitlab.com/datadrivendiscovery/common-primitives.git',
            ],
        },
        'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)))
        }],
        'python_path': 'd3m.primitives.data_preprocessing.one_hot_encoder.MakerCommon',
        'algorithm_types': [
            metadata_base.PrimitiveAlgorithmType.ENCODE_ONE_HOT,
        ],
        'primitive_family': metadata_base.PrimitiveFamily.DATA_PREPROCESSING,
    })

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)
        self._training_inputs: Inputs = None
        self._categories: Dict[int, Dict[int, Any]] = None
        self._fitted: bool = False

        # record unseen row index and column name
        self._unseen: List[Tuple[int, str]] = []
        self._missing: Dict[str, List[int]] = {}

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        self._training_inputs = inputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        inputs, column_to_use = self._select_inputs_columns(self._training_inputs)
        self._categories = {}
        seen_col_name: typing.Set = set()
        for column_index in column_to_use:
            column_name = inputs.columns[column_index]
            if column_name in seen_col_name:
                raise exceptions.ColumnNameError('Duplicated column name')
            seen_col_name.add(column_name)

            seen_values: Dict[int, Any] = {}
            for row_index, value in inputs.iloc[:, column_index].iteritems():
                if value in self.hyperparams['missing_values']:
                    if self.hyperparams['handle_missing_value'] == 'error':
                        raise exceptions.MissingValueError('Missing value in categorical data')
                    else:
                        continue
                if value not in seen_values.values():
                    seen_values[row_index] = value
            self._categories[column_index] = seen_values
        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        if not self._fitted:
            raise exceptions.PrimitiveNotFittedError("Primitive not fitted.")
        selected_inputs, columns_to_use = self._select_inputs_columns(inputs)
        if len(selected_inputs.columns[selected_inputs.columns.duplicated()].unique()):
            raise exceptions.ColumnNameError('Duplicated column name')
        columns_list = []
        one_hot_output = self._produce_one(inputs=selected_inputs, columns_to_use=columns_to_use)
        one_hot_output.metadata = inputs.metadata.select_columns(columns_to_use)
        one_hot_output.metadata = self._setup_metadata(one_hot_output)
        columns_list.append(one_hot_output)
        if self.hyperparams['handle_unseen'] == 'handle':
            extra_unseen_col = self._generate_unseen_col(inputs)
            extra_unseen_col.metadata = self._setup_unseen_col_metadata(extra_unseen_col.metadata)
            columns_list.append(extra_unseen_col)

        outputs = base_utils.combine_columns(
            inputs,
            columns_to_use,
            columns_list,
            return_result=self.hyperparams['return_result'],
            add_index_columns=self.hyperparams['add_index_columns'],
        )
        return CallResult(outputs)

    def get_params(self) -> Params:
        return Params(
            categories=self._categories,
            unseen=self._unseen,
            missing=self._missing,
            fitted=self._fitted
        )

    def set_params(self, *, params: Params) -> None:
        self._categories = params['categories']
        self._unseen = params['unseen']
        self._missing = params['missing']
        self._fitted = params['fitted']

    def _generate_unseen_col(self, inputs: Inputs) -> Outputs:
        extra_unseen_column_name = '{}{}{}'.format(self._Unseen_Column_Prefix, self.hyperparams['separator'],
                                                   self._generate_unique_col_name(inputs.columns.values))
        extra_unseen_column_values = np.zeros(inputs.shape[0], dtype=np.uint8)
        unseen_rows = [x[0] for x in self._unseen]
        mask = list(True if i in unseen_rows else False for i in range(0, inputs.shape[0]))
        extra_unseen_column_values[mask] = 1
        extra_columns = DataFrame(extra_unseen_column_values, columns=[extra_unseen_column_name], generate_metadata=True)
        return extra_columns

    def _setup_unseen_col_metadata(self, unseen_col_metadata: metadata_base.DataMetadata) -> metadata_base.DataMetadata:
        unseen_col_metadata = unseen_col_metadata.update_column(0, {'name': 'Unseen-Col'})
        unseen_col_metadata = unseen_col_metadata.update_column(0, {
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute',
                               'http://schema.org/Integer']})
        unseen_col_metadata = unseen_col_metadata.update_column(0, {'structural_type': int})
        return unseen_col_metadata

    def _setup_missing_value_col_metadata(self,
                                          missing_value_meta: metadata_base.DataMetadata) -> metadata_base.DataMetadata:
        missing_value_meta = missing_value_meta.update_column(0, {'name': ''})
        missing_value_meta = missing_value_meta.update_column(0, {
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute',
                               'http://schema.org/Integer']})
        missing_value_meta = missing_value_meta.update_column(0, {'structural_type': int})
        return missing_value_meta

    def _produce_one(self, *, inputs: Inputs, columns_to_use: typing.List[int]) -> Outputs:
        # convert discrete values to one-hot-vectors ...
        one_hot = []
        for column_index in columns_to_use:
            column_name = inputs.columns[column_index]
            columns = [self._make_onehot(column_name, self._categories[column_index], item, row_index) for
                       row_index, item in enumerate(inputs[column_name])]
            df = DataFrame(columns, generate_metadata=True)
            one_hot.append(df)
        result = pd.concat(one_hot, axis=1)

        return result

    def _setup_metadata(self, inputs: Inputs) -> metadata_base.DataMetadata:
        for column_index in range(0, inputs.shape[1]):
            inputs.metadata = inputs.metadata.update_column(column_index, {'semantic_types': metadata_base.NO_VALUE})
            inputs.metadata = inputs.metadata.update_column(column_index, {'name': inputs.columns.values[column_index]})
            inputs.metadata = inputs.metadata.update_column(column_index, {
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Attribute',
                                   'http://schema.org/Integer']})
            inputs.metadata = inputs.metadata.update_column(column_index, {'structural_type': metadata_base.NO_VALUE})
            inputs.metadata = inputs.metadata.update_column(column_index, {'structural_type': int})
        return inputs.metadata

    def _make_onehot(self, column_name: str, categories: typing.Dict, item: str, row_index: int) -> pd.Series:
        categories_list = list(categories.values())
        if self.hyperparams['handle_missing_value'] == 'column':
            missing_val_col_name = '{}-{}'.format(self._Missing_Column_Prefix,
                                                  self._generate_unique_col_name([column_name]))
            categories_list.append(missing_val_col_name)
        column_names = ['{}{}{}'.format(column_name, self.hyperparams['separator'], str(category)) for category in
                        categories_list]
        result = pd.Series(np.zeros(len(categories_list), dtype=np.uint8),
                           index=column_names)

        if item in self.hyperparams['missing_values']:
            self._missing.setdefault(column_name, []).append(row_index)
            if self.hyperparams['handle_missing_value'] == 'ignore':
                pass
            elif self.hyperparams['handle_missing_value'] == 'error':
                raise exceptions.MissingValueError(
                    "Encounter missing value in row: {}, col: {}".format(row_index, column_name))
            else:
                result.iloc[-1] = np.uint8(1)

        elif item in categories:
            result[categories[item]] = 1
        else:
            self._unseen.append((row_index, column_name))

            if self.hyperparams['handle_unseen'] == 'error':
                raise exceptions.UnexpectedValueError("Value {} not encountered during fit".format(item))
        return result

    def _generate_unique_col_name(self, column_names: typing.Iterable[str]) -> str:
        # Using sha256 to prevent extra column collide with other column names, overkill?
        return hashlib.sha256(
            bytes('.'.join(column_names), encoding='utf-8')).hexdigest()

    @classmethod
    def _get_inputs_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_inputs_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = base_utils.get_columns_to_use(
            inputs_metadata,
            hyperparams['use_inputs_columns'],
            hyperparams['exclude_inputs_columns'],
            can_use_column,
        )
        if not columns_to_use:
            raise ValueError("No inputs columns.")

        if hyperparams['use_inputs_columns'] and columns_not_to_use:
            cls.logger.warning("Not all specified inputs columns can used. Skipping columns: %(columns)s", {
                'columns': columns_not_to_use,
            })

        return columns_to_use

    @classmethod
    def _can_use_inputs_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        return \
            column_metadata['structural_type'] == str and \
            'https://metadata.datadrivendiscovery.org/types/CategoricalData' in column_metadata.get('semantic_types',
                                                                                                    [])

    def _select_inputs_columns(self, inputs: Inputs) -> Tuple[Inputs, List[int]]:
        columns_to_use = self._get_inputs_columns(inputs.metadata, self.hyperparams)

        return inputs.select_columns(columns_to_use), columns_to_use
